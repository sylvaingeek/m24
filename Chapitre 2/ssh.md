# SSH

Secure Shell est un protocole réseau cryptographique comprenant un serveur et un client. Désigné comme remplaçant de Telnet, car ce dernier était hautement non sécuritaire vu qu'il opérait en texte clair les rendant vulnérables à l'interception et la découverte en utilisant un analyseur de paquet. Le but de ce type de réseau est de s'assurer de la confidentialité et de l'intégrité des données sur un réseau non sécurisé tel qu'Internet.

Une des implémentations actuelles de SSH est la version OpenSSH créée par OpenBSD. Cette version de SSH est offerte par la majorité des distributions Linux, BSD et Windows. Le port utilisé généralement pour le protocole SSH est le 22 (TCP). Le protocole de connexion impose un échange de clés de chiffrement en début de connexion. Par la suite, tous les segments TCP sont authentifiés et chiffrés. Il devient donc impossible d'utiliser un sniffé pour voir ce que fait l'utilisateur.

Le SSH a une portion client et une portion serveur, illustrées ainsi:

![ssh](image/1920px-Client-server-model.png)

Sur Linux, le logiciel client se nomme ssh et le logiciel serveur se nomme sshd (d pour daemon ou service). Les fichiers de configuration du client et du serveur sont situés dans le répertoire /etc/ssh (ou sshd) généralement.

Pour installer le logiciel sous Debian: 

    sudo apt-get install openssh-server
    
Le service se configure comme tout autre service avec la commande:
  * systemctl enable / disable sshd (activation au démarrage)
  * systemctl start / stop sshd (démarre ou stoppe le service)

## Fonctionnalités

* Connexion shell distante et sécurisée
* Tunnel SSH (redirection d'un port TCP)
* SFTP (identique à sa contrepartie non sécurisée traditionnelle ftp)
* SCP
* rsync

## Méthode d'authentification:
 * Mot de passe (password)
 * Clé publique / privée (publickey)
 * Phrase secrète (passphrase)

### Clé privée / publique

Les authentifications avec clés fonctionnent avec deux clés:
 * une privée (nommée par défaut: id_rsa): C'est une clé qui ne doit jamais être divulguée à qui que ce soit.
 * une publique (nommée par défaut: id_rsa.pub): C'est cette clé que l'on copie sur les serveurs sur lesquels on veut se connecter.

### Généralisation de clé privée et publique

    ssh-keygen 

-t permet de spécifier le type de clés à générer: DSA, RSA.

-b permet de spécifier la taille.

Lors de la généralisation d'une clé, une des questions sera de générer une passphrase à l'intérieur de la clé afin d'encore mieux la sécuriser. Cette passphrase là sera à indiquer à chaque utilisation de la clé ssh.

[Voir ici pour plus de détails](https://man7.org/linux/man-pages/man1/ssh-keygen.1.html)

### Fonctionnement 

  * ssh-copy-id: automatise le processus suivant:

L'utilisateur copie la clé publique dans le répertoire .ssh/authorized_keys de son répertoire personnel sur le serveur qu'il tente d'accéder. Ceci lui indique les clés autorisées à se connecter à distance. Plusieurs serveurs en ligne permettent ce genre de configuration (Github, Azure ...). Github, par exemple permit alors de cloner ou pousser sans entrer le mot de passe Github à chaque utilisation. L'utilisateur peut utiliser la même clé SSH partout ou en générer une sur chaque connexion qu'il possède.

Par défaut ce sont les clés nommées id_rsa et id_rsa.pub qui sont utilisées, mais il est possible de les nommer différemment en répondant différemment à la question de ssh-keygen.

#### Exemple

* Créer l'utilisateur Michel sur l'ordinateur A et B.
* Générer le ssh-keygen sur l'ordinateur A en tant que Michel, ceci va générer dans le répertoire /home/michel/.ssh les clés publique et privée.
* Copier sur l'ordinateur B le contenu de la clé publique dans le fichier /home/michel/.ssh/authorized_keys 
* À partir de maintenant Michel de l'ordinateur devrait pouvoir se connecter sur le serveur B sans devoir entrer son mot de passe, car c'est une authentification par clé.
* Si vous créez des clés privées personnelles, elles ne doivent être accessibles seulement par vous (chmod 600) et sont enregistrées dans le répertoire .ssh.
 
### Utilisation possible

**SSH**

Logiciel de connexion à distante ssh (secure shell)
 
ssh user@server

**SCP**

Logiciel permettant la copie de fichier entre une source et une destination.

Voici la syntaxe traditionnelle d'un cp:

    cp source destination

En mode SCP, vous pouvez ajouter une notion de serveur et d'utilisateur.

    scp [user1@][ordi1:]emplacement1  [user2@][ordi2:]emplacement2

    user1 et user2 sont facultatifs, ce sera l'utilisateur en cours qui sera utilisé dans le cas où il n'est pas spécifié.

    ordi1 et ordi2 sont facultatifs, ce sera l'ordinateur en cours qui sera utilisé dans le cas où il n'est pas spécifié.


**SFTP**

Dans le passé, le FTP était un protocole voué au transfert de fichier entre un client et un serveur.

Selon Wikipedia:

File Transfer Protocol (protocole de transfert de fichier), ou FTP, est un protocole de communication destiné au partage de fichiers sur un réseau TCP/IP. Il permet, depuis un ordinateur, de copier des fichiers vers un autre ordinateur du réseau, ou encore de supprimer ou de modifier des fichiers sur cet ordinateur. Ce mécanisme de copie est souvent utilisé pour alimenter un site web hébergé chez un tiers.

FTP obéit à un modèle client-serveur, c'est-à-dire qu'une des deux parties, le client, envoie des requêtes auxquelles réagit l'autre, appelé serveur. En pratique, le serveur est un ordinateur sur lequel fonctionne un logiciel lui-même appelé serveur FTP, qui rend publique une arborescence de fichiers similaire à un système de fichiers UNIX. Pour accéder à un serveur FTP, on utilise un logiciel client FTP (possédant une interface graphique ou en ligne de commande).

Par convention, deux  ports sont attribués (well known ports) pour les connexions FTP : le port 21 pour les commandes et le port 20 pour les données.

Le SFTP a été une mise à jour de ce service: il permet de permettre l'échange de fichier tout en étant entièrement crypté entre le client et le serveur. 

    sftp user@server
    Les commandes de base d'un serveur FTP sont les suivantes:

    cd (répertoire)
    get (télécharger un fichier du serveur vers le client)
    put (télécharger un fichier du client vers le serveur)

    Toujours considérer que le client est l'ordinateur sur lequel vous êtes situés.

**Tunnel SSH**

Permet de rediriger le trafic d'un port vers un autre en le passant par un tunnel crypté.

Voir les exemples sur le site de ssh.com:

https://www.ssh.com/ssh/tunneling/example

ssh -L [LOCAL_IP:]LOCAL_PORT:DESTINATION:DESTINATION_PORT [USER@]SSH_SERVER

ssh -R [REMOTE:]REMOTE_PORT:DESTINATION:DESTINATION_PORT [USER@]SSH_SERVER