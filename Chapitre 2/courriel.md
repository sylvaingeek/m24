# Courriel

Le service de courrier électronique (appelé également «Courriel» ou encore «E-Mail» en anglais) permet d'envoyer des messages électroniques via l'Internet.

Avantages:
* Il est virtuellement gratuit (si l’on se situe du côté utilisateur)
* Il est généralement rapide (de nos jours, il est quasi instantané et souvent utilisé lors de l'inscription)
* Il est écologique (...)

Inconvénients:
* Signatures électroniques sur des documents ayant une portée légale.
* Souvent non sécuritaire, il est impossible de prouver hors de tout doute que l'expéditeur est bien celui que vous croyez. À moins qu'il soit signé par une clé et que le serveur d'approbation de clé soit honnête.
* Quiconque ayant accès aux serveurs entre l'expéditeur et le destinataire peut lire le courriel à moins que le courriel soit chiffré.

## Fonctionnement

### Client

Pour utiliser le courriel sur l'Internet, un usager doit:
* disposer d'un compte de courriel généralement chez son FSI ou fourni par l'entremise de son travail ou l'équivalent d'une boîte aux lettres électronique à laquelle est associée une adresse ayant le format suivant : codeusager@nomdedomaine.
* peut disposer d'un logiciel client de courrier électronique par exemple Outlook pour le courriel conventionnel
* peut disposer d'une interface graphique chez un fournisseur spécialisé de service de courriel (Live, Gmail)

### Serveur
Pour utiliser le courriel sur l'Internet, un serveur doit:
* offrir un serveur de courriel sortant SMTP (Simple Mail Transport Protocol) : acheminer les messages envoyés par nos clients (vers d'autres destinataires) et recevoir les messages destinés à nos clients de destinataires situés sur d'autre serveur. **C'est le serveur pointé par le MX record dans le fichier zone DNS.**
* offrir un serveur de courriel entrant (POP ou IMAP) : écouter le port approprié pour une requête de demande de nouveau courriel par l'entremise d'un usager.
* offrir un espace disque pour stocker le contenu des courriels reçus pour les usagers.

## Protocoles

### POP, Post Office Protocol

C'est un protocole qui permet de récupérer les courriers électroniques situés sur un serveur de messagerie électronique.

### IMAP, Internet Message Access Protocol

C'est un protocole qui permet de récupérer les courriers électroniques situés sur un serveur de messagerie électronique.

**La différence principale entre les deux repose dans le fait que le IMAP laisse les courriels sur le serveur afin de réduire la bande passante nécessaire pour voir ses courriels, que l'utilisateur peut choisir ceux qu'il souhaite lire, qu'on peut y faire des dossiers pour trier ses courriels ...**

## Acheminement

Voici le trajet qu'effectue un courriel envoyé de gaston@sympatico.ca à michel@videotron.ca
 1. Le courriel est écrit dans un logiciel client (un éditeur de texte tel que mutt)
 1. Le courriel est envoyé vers le serveur SMTP du FSI: smtp.sympatico.ca
 1. Le serveur SMTP de Sympatico inspecte le destinataire du courriel et regarde s'il est situé sur son serveur (dans le cas où nicole@sympatico.ca écrit à gaston@sympatico.ca)
 1. Le serveur SMTP de Sympatico contacte le serveur DNS de Vidéotron afin de déterminer l'adresse IP du serveur MX. (MX est record est l'entrée DNS pour le Courriel eXchange)
 1. Le serveur SMTP de Sympatico établit une connexion au serveur SMTP de Vidéotron: smtp.videotron.ca par exemple.
 1. Le courriel est enregistré dans un des comptes du serveur Vidéotron, en occurrence la boîte Michel.

Lorsque Michel va se connecter à sa boîte par l'entremise de son logiciel de lecture de courriel préféré:
 1. Il va effectuer une requête POP ou IMAP à sa boîte de courriel.
 1. Le courriel va être téléchargé localement et supprimé selon les configurations du logiciel de courriel.

### À savoir

Il est à noter qu'il est toujours possible de créer et de gérer nous-mêmes nos propres serveurs SMTP et POP, afin d'envoyer et recevoir des courriels. Cependant il devient de plus en plus difficile de s'acquitter de cette tâche, car il faut obtenir ASSEZ DE renommée afin de s'assurer que nos courriels ne se retrouvent pas dans la boîte JUNK étant que la source (vos serveurs)n'est pas correctement constituée:

C'est pour cette raison qu'existent de multiples API de service de messagerie:
  * mailgun
  * sendgrid
  * mailjet
  * ...

## Sécurité des courriels

Tout comme le courrier traditionnel, il est possible de vous envoyer une lettre falsifiant l'expéditeur. Étant donné qu'une grande portion du piratage provient du courriel, 3 données ont été ajoutées pour permettre une meilleure protection pour l'envoi de courriel: SPF, DKIM et DMARC.

### SPF
Sender Policy Framework (SPF) est un mécanisme qui permet au travers du serveur DNS de spécifier quelles adresses IPs sont permises à envoyer le courriel découlant de ce domaine.

Si on fait une analogie avec le courrier, ce serait comme si on pouvait contacter l'expéditeur pour demander si le facteur Tom peut livrer votre courrier provenant de lui.

### DKIM
DomainKeys Identified Mail (DKIM) est un mécanisme qui permet de spécifier la responsabilité pour un message et de le protéger en lui apposant une signature digitale:
 * Le contenu n'a pas été modifié
 * Les entêtes du courriel n'ont pas été modifiés depuis le courriel original (et qu'il y en a pas de nouveau)
 * L'expéditeur est propriétaire du domaine DKIM ou est autorisé par ce dernier.

Si on fait une analogie avec le courrier, l'enveloppe a un sceau d'authenticité qui prouve que l'enveloppe et son contenu n'ont pas été altérés et que le sceau provient de l'expéditeur de l'enveloppe.

### DMARC
Domain-based Message Authentication, Reporting, and Conformance (DMARC) est un mécanisme basé sur SPF et DKIM. Il valide le résultat du SPF et du DKIM  et que l'entête (Header From) du courriel provient du même domaine que ceux utilisés dans le SPF et dans le DKIM. Le Header From est l'adresse de l'expéditeur que l'on voit dans notre outil de courriel.

Si une des validations échoue, on doit honorer la politique DMARC qui est stipulée. Celle peut indiquer de mettre en quarantaine (p=quarantine), de rejeter (p=reject) ou d'ignorer et de livrer le courriel (p=none)

Tout comme le vrai courrier, la signature du courriel n'a pas à matcher celui qui vous envoie le courriel. Le problème c'est qu'avec le courriel, l'enveloppe n'est pas toujours visible au destinataire.

