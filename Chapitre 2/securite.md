# Fonction de hachage
Une fonction de hachage cryptographique est une fonction de hachage qui, à une donnée de taille arbitraire, associe une image de taille fixe et une propriété essentielle à cette image : elle est pratiquement impossible à inverser.

 **Si la résultante d'une donnée par la fonction se calcule très efficacement, le calcul inverse d'une donnée d'entrée ayant pour résultante une certaine valeur se révèle impossible sur le plan pratique. Pour cette raison, on dit d'une telle fonction qu'elle est à sens unique.**

Une fonction de hachage cryptographique idéale possède les quatre propriétés suivantes :
  1. la valeur de hachage d'un message se calcule très rapidement

et pour une valeur de hachage donnée:

  2. impossible de construire un message ayant cette valeur de hachage
  3. impossible de modifier un message sans en changer sa valeur de hachage
  4. impossible de trouver deux messages différents ayant la même valeur de hachage

Voir md5sum, sha256sum, sha512sum ...

## Application

### Intégrité des fichiers

La modification d'un fichier lors d'une transmission peut être prouvée en comparant la valeur de hachage du fichier avant et après la transmission. En pratique, la plupart des algorithmes de signature numérique d'un fichier ne vérifient pas que le fichier n'a pas été changé, mais bien que la valeur de hachage du fichier n'a pas changé. Toutefois, à cause des caractéristiques des fonctions de hachage cryptographiques, la vérification de la valeur de hachage est considérée comme la preuve que le fichier lui-même est authentique. 

**Des valeurs de hachage obtenues par les algorithmes SHA256, BLAKE2b, SHA1, MD5 sont parfois affichées avec les fichiers correspondants sur des sites Web ou des forums informatiques pour permettre la vérification de l'intégrité des fichiers.**

Voir https://www.archlinux.org/download/

Cette pratique établit une chaîne de confiance pourvu que les valeurs de hachage soient affichées sur un site sécurisé par HTTPS sinon elles pourraient être modifiées par l'entremise de tous ceux qui transportent les différents paquets.

### Vérification de mot de passe

Une application associée au hachage cryptographique est la vérification de mot de passe inventé par Roger Needham. Le stockage de tous les mots de passe utilisateur en clair peut entraîner une violation de sécurité massive si un fichier de mots de passe est compromis ou une table d'une base de données est accessible. Une façon de réduire ce danger est de stocker seulement la valeur de hachage de chaque mot de passe. Pour authentifier un usager, le mot de passe fourni par l'utilisateur est haché et comparé avec la valeur de hachage stockée. Avec cette approche, les mots de passe perdus ne peuvent pas être récupérés s'ils sont oubliés; ils doivent être remplacés par de nouveaux mots de passe.

Le mot de passe est souvent concaténé avec un sel cryptographique non secret avant que la fonction de hachage soit appliquée. Le sel est stocké avec la valeur de hachage du mot de passe. Puisque les utilisateurs ont différents sels, il est impossible de construire des tables de valeurs de hachage précalculées pour les mots de passe communs. Les fonctions de dérivation de clés, comme PBKDF2, bcrypt ou scrypt, typiquement utilisent des appels répétés d'une fonction de hachage cryptographique pour augmenter le temps nécessaire pour effectuer des attaques par force brute sur des valeurs de hachage de mots de passe stockés. 

Voir https://xkcd.com/936/

### Gestion de version
Une valeur de hachage peut aussi servir comme un moyen d'identifier de manière fiable un fichier ou plutôt déterminer s'il y a eu un changement dans un fichier:

**Plusieurs systèmes de gestion de code source tel Mercurial et Monotone, utilisent le SHA1 de divers types de contenu (le contenu de fichiers, les arborescences de répertoires, etc.) pour les identifier de manière unique. Git est rendu à sha256sum depuis 2019**

Les valeurs de hachage sont utilisées pour identifier les fichiers sur les réseaux de partage de fichiers pair-à-pair. Par exemple, dans un lien ed2k, la valeur de hachage d'une variante de MD4 est combinée avec la taille du fichier, fournissant des informations suffisantes pour localiser les sources du fichier, le télécharger et en vérifier le contenu.

Étant des fonctions de hachage d'un genre particulier, les fonctions de hachage cryptographiques se prêtent aussi à cette utilisation. Cependant en comparaison avec les fonctions de hachage standards, les fonctions de hachage cryptographiques sont beaucoup plus coûteuses en calcul. Pour cette raison, elles sont utilisées seulement dans des contextes où il est nécessaire de se protéger contre les risques de falsification.

# Cryptographie asynchrone

Utilise une paire de clé:
* une clé privée
* une clé publique

La clé publique peut être partagée avec n'importe qui, mais la clé privée doit rester en possession de son propriétaire. Un message chiffré ne peut être déchiffré qu'en utilisant sa clé associée. Une grande partie de la cryptographie repose sur ce principe de cryptographie asynchrone. Nous avons vu déjà l'utilisation de SSH afin de générer.

## PGP
PGP se propose de garantir la confidentialité et l'authentification pour la communication des données. Il est souvent utilisé pour la signature de données, le chiffrement et le déchiffrement des textes, des courriels, fichiers, répertoires et partitions de disque entier pour accroître la sécurité des communications par courriel. Utilisant la cryptographie asymétrique mais également la cryptographie symétrique, il fait partie des logiciels de cryptographie hybride.

### Encryption
Comment faire pour envoyer un message à Gilbert sans que quiconque puisse le lire?
On utilise la clé publique de Gilbert pour chiffrer le message 
On envoie le message chiffré à Gilbert.
La seule personne ayant la clé de décodage de Gilbert est Gilbert, c'est sa clé privée.
Gilbert lit déchiffre le message.
Gilbert lit le message.

### Intégrité des messages
Un autre avantage est l'intégrité des messages, comment s'assurer que le message suivant en clair ne peut être modifié par qqun? 

**Bonjour j'envoie un chèque de 100$ à Paul.**

Si une personne malhonnête nommée Manon intercepte ce message et elle modifie le destinataire et le montant: Manon et 1000$. 

Comment régler se problème?

**En utilisant ta clé privée pour encoder le message.**

Comme tout le monde peut avoir accès à ta clé publique, les messages envoyés par vous peuvent être chiffrés avant d'être envoyée (par votre clé privée). Personne ne pourra alors modifier votre message. Pourquoi? 

* il pourra le déchiffrer, 
* il pourra le lire 
* il pourra le modifier 
* **mais ne pourra pas le chiffrer à nouveau, car il n'aura pas accès à votre clé privée.**

L'inverse est aussi vrai, en obtenant la clé publique de votre ami, les seuls messages que cette clé publique pourrait décrypter sont les messages cryptés de votre ami.

### Confiance

Vous pouvez aisément créer des clés privées et publiques, les protéger par un mot de passe et les téléverser sur un serveur de clé. Cette pratique permet de télécharger la clé publique des gens avec qui vous communiquer et leur permettre de télécharger la vôtre pour décrypter vos messages. Le plus important est de vraiment protéger vos clés privées.

Comment s'assurer que la clé appartient bien à la personne lorsque vous la téléchargez d'un serveur PGP donné

Vous pourriez faire semblant de vous nommer François Legault et mettre votre clé publique sur un serveur de clés données. PGP a également la possibilité de signer une clé pour un ami, donc plus que votre clé est signée par vos amis / connaissances / personnes de confiance, plus il y a de chance que la personne soit bien cette personne.

### Limite de PGP
PGP se concentre surtout sur le contenu du message. Cependant, même si le contenu reste illisible, le fait d'envoyer un courriel vers certaines personnes peut être jugé compromettant. De plus, dans certains pays, il peut être illégal d'utiliser PGP. Vous n'êtes pas obligé de téléverser vos clés PGP en ligne, vous pouvez les garder que pour vous et les envoyer aux gens avec qui vous communiquez et avoir votre propre réseau de confiance.