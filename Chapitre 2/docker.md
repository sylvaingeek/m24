# docker
Docker est un programme virtuel qui permet de faire de la virtualisation au niveau du système d'exploitation et non de la machine. Ce dernier fut mis en place en 2013 et développé par l'entreprise Docker inc.

Concrètement, Docker est utilisé pour gérer des conteneurs. Chaque conteneur est isolé et contient son ensemble de librairies, d'outils et de configurations pour son application. Tous les conteneurs sont gérés par le même noyau donc il est plus léger que les machines virtuelles. Un conteneur a besoin d'une image pour fonctionner et peut donc découler d'un repository d'images publiques.

De nos jours, Amazon AWS ECS2 et Google proposent leur propre système de gestion des containeurs.

## Plateforme Docker

Docker permet de créer des packetages et de rouler des applications dans un environnement isolé nommé un conteneur. L'isolation et la sécurité vous permet de rouler plusieurs conteneurs en simultanées sur un hôte chacun isolé les uns des autres.

Contrairement aux machines virtuelles, les conteneurs sont légers, ils n'émulent pas un système d'exploitation complet, et ils contiennent tout ce qui est nécessaire pour rouler l'application (rien ne provient de l'hôte lors de l'exécution). Vous pouvez facilement partagez les conteneurs.

Docker vous offre les outils pour gérer et gérer la vie de vos conteneurs.

### Architecture Docker
Docker utilise une architecture client / serveur. Le client Docker communique avec le daemon Docker qui s'occupe à créer, exécuter et à distribuer les conteneurs docker. Le client et le serveur Docker peuvent être exécuté sur la même machine ou vous pouvez connecter votre client vers un serveur Docker distant.

![architecture](image/architecture.svg)

## Docker Architecture Diagram

### Le démon Docker (serveur)
Le démon docker (dockerd) écoute pour les différentes requêtes du client et gère les objets Docker en conséquence: images, conteneurs, réseaux et volumes.

### client Docker
Le client docker (docker) est la façon principale que vous communiquez avec docker. Quand vous utilisez une commande tel que docker run, le client envoie la demande à dockerd, qui l'exécute. Le client docker peut communiquer avec plus qu'un démon.

### registre Docker
Un registre docker stocke les différentes images docker. Docker Hub est un registre public que n'importe qui peut utiliser et par défaut docker est configuré pour rechercher les images sur le Docker Hub. Vous pouvez utiliser votre propre registre privé si vous le désirez.

Quand vous utilisez docker pull ou docker run , l'image est alors téléchargée de vos registres. Lorsque vous utilisez docker push , votre image est téléversée vers vos registres.

### image Docker
Une image est un gabarit (template) en lecture seulement avec des instructions afin de créer un conteneur Docker. Souvent une image est basée sur une autre image avec certaines personnalisations. Par exemple, vous pouvez bâtir une image basée sur ubuntu, vous y ajoutez le serveur web apache et votre application, de plus, certaines configurations afin de permettre l'exécution de votre application.

Vous pouvez vous créer vos propres images ou uniquement utilisé celles fournies par d'autres. Pour créer vos propres image, vous pouvez utiliser un Dockerfile qui ressemble à un script Bash afin de définir les différentes étapes afin de créer une image Docker et de l'exécuter. Chaque instruction crée une couche dans l'image, lors de changement et que vous devez recréer votre image, seulement ceux ayant été modifiés sont recompilés. Ceci permet de créer des images légères, petites et rapides.

### conteneur Docker
Un conteneur est une instance exécutable d'une image. Vous pouvez créer, démarrer, arrêter, déplacer ou effacer un conteneur en utilisant un API ou la ligne de commande (CLI). Vous pouvez le relier à un ou plusieurs réseaux, attacher du stockage et même créer une nouvelle image en fonction de son état actuel.

Par défaut, un conteneur est assez bien isolé des autres conteneurs de la machine l'exécutant. Vous pouvez contrôler son isolation par rapport au réseau ou au stockage des autres conteneurs.

Un conteneur est défini par son image et par les options de configurations que vous lui avez donné lors de sa création ou de son exécution.

## volume Docker

Un volume est la façon de stocker des données persistentes dans les conteneurs. Ce qu'il faut comprendre ici c'est que les données sont persistentes tant que le docker est en utilisation, dès qu'il est éteint, les modifications et les données seront perdues.

Utilisation de base

docker run image

En lançant l'exécution ainsi, que ce passe-t'il?
```
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
b8dfde127a29: Pull complete
Digest: sha256:f2266cbfc127c960fd30e76b7c792dc23b588c0db76233517e1891a4e357d519
Status: Downloaded newer image for hello-world:latest
Hello from Docker!
This message shows that your installation appears to be working correctly.
To generate this message, Docker took the following steps:
1. The Docker client contacted the Docker daemon.
2. The Docker daemon pulled the "hello-world" image from the Docker Hub. (amd64)
3. The Docker daemon created a new container from that image which runs the executable that produces the output you are currently reading.
4. The Docker daemon streamed that output to the Docker client, which sent it to your terminal.

To try something more ambitious, you can run an Ubuntu container with:

Et finalement le run, quit et l'exécution est terminéee. Que se passera-t'il à l'exécution de cette commande?
$ docker run -it ubuntu bash
...
```

Comme expliquée ci-dessus: l'image est téléchargée, le conteneur est créé, l'output est envoyé vers votre sortie standard, l'image est arrêtée.

### Commandes principales

#### run

Exécute une commande dans un nouveau conteneur

#### start 

Démarre un ou plusieurs conteneurs arrêté

#### stop

stoppe un ou plusieurs conteneur démarrés

#### build

bâtit une image à partir d'un Dockerfile

#### pull

Télécharge une image à partir d'un registre

#### push

Pousse une image vers un registre

#### exec

Exécute une commandae dans un conteneur étant en cours d'exécution

#### commit

Crée une nouvelle image à partir des changements réalisés sur un docker

### Options lors de la création de conteneurs
-p permet de partager un port entre la source (host) et la destination (conteneur). Dans le sens, que si votre hôte reçoit une requête vers le port 80, elle sera redirigée vers votre conteneur.

-v permet de partager un répertoire entre la source x (host) et la destination y (conteneur). Les fichiers du répertoire x seront disponibles à l'emplcement y.

## Dockerfile
Permet la création d'image qui peuvent être poussée ou non vers un registry. Il utilise une image existante et effectue certaines modifications avant de finaliser l'image.

Pour bâtir une image à partir d'un Dockerfile vous utilisez la commande:

docker build -t "nom_image" . (. pour le répertoire en cours)

Quelques exemples sont disponibles sur le git du cours, sinon vous pouvez en trouver une multitude sur github, voici quelques exemples: 

https://github.com/jessfraz/dockerfiles/blob/master/powershell/Dockerfile

https://github.com/jessfraz/dockerfiles/blob/master/rdesktop/Dockerfile

Ceci permet donc d'installer powershell ou remotedesktop sur votre ordinateur sans devoir rien modifier à votre hôte original.