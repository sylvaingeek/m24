$StartTime=(Get-Date).AddDays(-1)
$EndTime=(Get-Date)

Get-WinEvent -FilterHashtable @{LogName='Application'; StartTime=$StartTime; EndTime=$EndTime; } -MaxEvents 5 | Select-Object TimeCreated, ID, Message