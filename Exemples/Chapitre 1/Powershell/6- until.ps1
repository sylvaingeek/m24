do {
  $demande = Read-Host -Prompt "Entrez la taille limite pour votre nombre Random (0 pour quitter)"
  if ($demande -gt 0) {
    Get-Random -Maximum $demande 
  }
} until ($demande -eq 0)