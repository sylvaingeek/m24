function Show-Info() {
  [CmdletBinding()]
  Param(
    [Parameter(Mandatory=$true)]
    [string]$ordinateur,
    [string]$repertoire,
    [int] $nombre
  )

  Write-Output "Ordinateur: $ordinateur"
  Write-Output "Repertoire: $repertoire"

  Write-Verbose "J'adore les patates"
  Write-Verbose "$nombre"

  Write-Debug "Allo!"
}

