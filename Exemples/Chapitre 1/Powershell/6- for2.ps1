$colors = @("Green","Cyan","Red","Magenta","Yellow","White", "Black", "DarkYellow", "DarkBlue")
for ($x='' ;$x.length -le 30;$x=$x+'x'){
    $fgcolor = $colors | Get-Random
    $bgcolor = $colors | Get-Random
    Write-Host $x -ForegroundColor $fgcolor -BackgroundColor $bgcolor
    Start-Sleep -Milliseconds 200
}