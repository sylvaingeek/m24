while (($maxValue = Read-Host -Prompt "Entrez une limite pour votre nombre Random (0 pour quitter)") -ne 0) {
    $value = (Get-Random -Maximum $maxValue)

    Write-Output ($value + 1)
    Write-Debug ("Nous avons genere un nombre entre 0 et $($maxValue-1) : $value")
}

# Pour voir Write-Debug il faut:
# $DebugPreference = 'continue'