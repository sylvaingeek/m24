#!/bin/bash

case $LANG in
  en*) echo 'Hello!'
       ;;
  fr*) echo 'Salut!'
       ;;    
  de*) echo 'Guten Tag!'
       ;;
  nl*) echo 'Hallo!'
       ;;    
  it*) echo 'Ciao!'
       ;;
  es*) echo 'Hola!'
       ;;
  C|POSIX) echo 'hello world' 
       ;;
  *)   echo 'Je ne parle pas votre langue.' 
       ;;
esac