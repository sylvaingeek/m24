#!/bin/bash
# Méthode addition, reçoit deux chiffres en paramètres et additionne les deux
# ne fonctionne qu'avec deux arguments
# Auteur: Michel Di Croci
# 8 avril 2020

function addition
{
    local a                 # a est local
    ((a=$1+$2))             # $1 et $2 sont globaux
    echo "$1 + $2 = $a" 
    return 0
}

addition $1 $2
echo "Valeur de sortie d'exécution $?" #Valeur de sortie de la fonction
echo "Valeur de a: $a" # bien sûr elle est locale
