# BASH

[Installation](Installation.md)

[git](git.md)

[Introduction](Introduction.md)

[Commande](Commande.md)

[Assignation et type](Assignation.md)

[Sortie](Sortie.md)

[Opérateur](Opérateur.md)

[Boucles](Boucles.md)

[Syntaxe des chaînes](Syntaxe.md)
