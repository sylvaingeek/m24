# Sortie d'exécution

Toutes les commandes résultent en un code de statut de sortie lorsqu'elle se termine. Le code de sortie permet d'être utilisé par n'importe quelle autre application subséquente afin de déterminer si la dernière commande a été un succès ou non. La convention veut qu'un succès soit identifié par un 0. Le reste des nombres se situe entre 1 et 255 est déterminé comme un échec.

Par exemple, l'application ping qui permet de déterminer si un serveur est en ligne et répond au requête ping. Un 0 signifiera que les pings se sont bien envoyés, tout problème sera affiché en haut de 0. Ce qui permet facilement de tester si un serveur est en ligne ou non.

Comment voir le code de sortie?
La façon la plus simple d'afficher la variable $?

    echo $?

Cela devrait afficher 0, nous reviendrons plus tard sur les variables disponibles en BASH.

    grep '[pv]ain' fichier.txt
    echo $?

Relancer votre grep avec un s au lieu du n et votre code de sortie sera un 1:

    grep '[pv]ais' fichier.txt
    echo $?

## Sortie standard

Chaque système Linux a une sortie par défaut pour que chaque programme qui s'exécute puisse écrire sa sortie (ses echos"). On la surnomme carrément stdout. À chaque fois que l'on fait un echo, la commande envoie à stdout. Le shell surveille le stdout et quand il voit du nouveau contenu apparaître, il l'affiche à l'écran.

### Redirection
La redirection est de rediriger la sortie par exemple stdout vers un autre emplacement (souvent un fichier). Donc la commande au lieu d'afficher à l'écran les messages de fonctionnement, ils seront dans un fichier par exemple.

Pour ce faire, nous utilisons le caractère > et indiquons l'emplacement où envoyer le contenu.

    ls > ls.txt

La sortie de la commande ls va être transféré dans le fichier ls.txt. Brève attention à faire avec cette commande, si le fichier existe, son contenu sera effacé pour être remplacé par le contenu du ls.

Il est également possible de faire ceci:

     ls >> ls.txt
La sortie ajoute le contenu du ls au contenu du fichier ls.txt, si le fichier n'existe pas, il va le créer avant d'y ajouter le contenu vide.

## Sortie erreur

Exactement comme le standard output, mais c'est les messages d'erreur qui se retrouvent ici. On peut facilement en constater l'affichage en faisant un cat sur un fichier inexistant:

    cat xyz.txt

Ça indique ceci:

    cat: xyz.txt: Aucun fichier ou dossier de ce type

CQFD!

Bon vous pourriez êtes tenté de me dire, comment savoir si ce n'est pas le standard output et vous auriez emplement raison. Nous allons redirigez la sortie vers un fichier et afficher le contenu du fichier:

    cat xyz.txt > patate.txt
    cat patate.txt

## Entrée Standard

Standard Input ou stdin est la place par défaut où les commandes attendent pour de l'information. Avez vous déjà essayé de taper cat sans arguments? Cela va faire qu'il va attendre que vous écriviez des trucs et les répéter lorsque vous appuyez sur enter.

Il écoute sur stdin avant de les afficher sur stdout. Vous pouvez terminer l'affichage de ceci en entrant ctrl-D.

La commande rattachée à stdin est la suivante:

    wc -l < fichier_long.txt

Ça prend le contenu de fichier_long.txt et ça l'envoie au stdin de wc -l (qui est un compteur, avec l comme argument pour compté le nombre de ligne).

## File descriptor
Un descripteur de fichier ou "File Descriptor" est un entier identifiant une source d'entrée ou de sotie. Par exemple, stdin est 0, stdout est 1 et stderr est 2. Ce ne sont pas des nombres aléatoires, ils sont définis dans le standard POSIX, il s'agit en fait d'un standard pour les système d'exploitation de type POSIX.

Pour dubliquer la sortie vers un FD, on utilise l'opérateur >& et le numéro de la sortie, par exemple:

```bash
# Rediriger stdout à stdout (FD 1)

$ echo "allo" >&1
allo

# Rediriger stdout à stderr (FD 2)

$ echo "allo" >&2
allo

# Rediriger les erreurs dans la sortie standard

$ cut -d , -f 1 fichier_inexistant.csv > eleves.txt 2>&1
```

## Pipes
Les pipes sont des façons d'envoyer le contenu de la portion stdout d'une commande vers la portion stdin d'une seconde commande. Vous voyez ici la puissance de Unix à son meilleur: Unix n'a jamais voulu tout refaire au complet, chaque portion avait un rôle et on préférait divisier pour mieux régner.

    echo "bonjour les amis" | sed "s/bonjour/allo/"     

La sortie stdout s'en va dans la commande stdin de sed qui remplace le contenu, il recherche la première occurence de bonjour et la remplace par allo.

[Opérateur](Opérateur.md)