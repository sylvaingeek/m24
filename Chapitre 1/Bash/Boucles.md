# Boucles

1. for variable in liste: Pour chaque élément variable de liste
1. for (( initialisation; test; incrémentation )): do code done
1. while: Répéter aussi longtemps que commande a un code d'exécution égal à 0
1. until: Répéter aussi longtemps que commande a un code d'exécution autre que 0

Par exemple:

```bash
for i in {10..1}
> do echo "$i canettes de bière bues."
> done

10 canettes de bière bues.
9 canettes de bière bues.
8 canettes de bière bues.
7 canettes de bière bues.
6 canettes de bière bues.
5 canettes de bière bues.
4 canettes de bière bues.
3 canettes de bière bues.
2 canettes de bière bues.
1 canettes de bière bues.
```

Chaque boucle commence par le mot clé do, possède quelques commandes dans son corps et se complète par le mot clé done.

En pratique, l'utilisation des boucles a différentes tâches. Le for est généralement utilisé lorsque nous savons le nombre d'élément dans une liste et que nous voulons parcourir la liste séquentiellement. La boucle while est utilisée lorsque nous ne savons pas combien de fois nous allons devoir répéter le code que nous avons à parcourir.

for
Vous avez déjà vu la commande utilisée dans l'exemple précédent. Ceci permet de faire une boucle (bien sûr) que sur un ensemble d'élément. C'est surtout utilisé lorsqu'on veut faire une boucle sur un ensemble de fichier.

```bash
for i in 10 9 8 7 6 5 4 3 2 1
> do echo "$i canettes de bières vides."
> done
```

Ou encore:

```bash
$ for i in {10..1}
> do echo "$i canettes de bière bues."
> done
```

Finalement, on avait également vu que c'est plus simple de naviger dans les variables tableaux avec cette syntaxe :

```bash
$ for nom in "${noms[@]}"; 
>  do
>     echo "$nom"; 
>  done
```

Ne pas oublier la substitution de commande, c'est vraiment ce qui est le plus utile en bash:

```bash
FICHIERS=/var/html/*
for f in $FICHIERS
do
  echo "Traitement du fichier: $f..."
  cat $f
done
```

La seconde syntaxe ressemble plus à la boucle de for que vous connaissez dans les langages de programmation (comme en Java):

```bash
for (( i=10; i > 0; i-- ))
> do echo "$i canettes de bière bues."
> done
```

L'important est de noter la présence de la portion do et done. Chaque do se termine par un done.

Voici ce que je vous expliquais plus tôt par rapport à l'utilisation de for dans une boucle pour des fichiers:

```bash
mkdir backup
for fichier  in *.jpg
% > do cp "$fichier" backup/
% > done
```

Éviter de faire des déplacements dans les répertoires (cd) lorsque vous faîtes des boucles comme ça. Je copie chaque fichier .jpg vers le dossier backup.

N'oubliez jamais d'encadrer correctement vos variables, sinon vous auriez des surprises avec les espaces.

Vous pouvez joindre les 3 derniers éléments d'information

```bash
for f in *; do
    if [ -d "$f" ]; then
        # $f is a directory
    fi
done
```

Vas lister tous les fichiers et répertoires du dossier en cours, vas tester pour savoir si l'élément est une répertoire et si c'est le cas, va affichier l'information.

Une autre façon de faire aurait été d'utiliser la commande find, qui permet de lister les éléments situé sous lui:

    find . -type d -maxdepth 1

Comme vous pouvez le constater, il n'y a généralement pas une seule façon de jouer avec Bash. Le tout est de rester le plus "simple" possible: pas de code obscur, si vous ne comprenez pas un code sur Internet, évitez le. 

Personnellement, j'évite tout code que je ne comprends pas, donc si vous n'êtes pas en mesure de comprendre du code, de graçe ne le copier pas comme résultat.

## while

Une boucle qui s'exécute tant que le test de while est vrai:

```bash
while true
> do echo "Boucle infinie"
> done
```

Dans ce cas-ci, un for aurait été plus approprié:

```bash
$ (( i=10 )); while (( i > 0 ))
> do echo "$i canettes de bière bues."
> (( i-- ))
> done
```

Rappelez-vous le code de sortie d'une commande, alors on peut boucler sur ça:

```bash
while ! ping -c 1 -W 1 1.1.1.1; do
> echo "en attente pour 1.1.1.1"
> sleep 1
> done
```

Donc tant que le ping ne fonctionne pas (n'oubliez pas que la valeur de retour d'un ping sera > 0 pour tout ping non fonctionnel. En inversant avec le !, on a une boucle qui va s'exécuter seulement lorsque les pings ne fonctionneront pas.

```bash
while read -p 'La machine à bonbon.\n Insérer 20c et entrer votre nom: ' nom
> do echo "La machine à bonbon donne 3 bonbons à $nom."
> done
```

Ici on pose une question, tant que la lecture se produit correctement, on continue la boucle

Pour permettre une sortie de la boucle, on peut utiliser le break:

```bash
while read -p 'Nom: ?' nom;
do
  if [ -z $nom ] ; then
     break
  else
     echo "on donne un bonbon à $nom"
  fi
done
```

## until

Le until est exactement (exactement) comme un while, la seule différence : la condition doit être fausse pour que le until continue à boucler.

```bash
until ping -c 1 -W 1 192.168.88.1
> do echo "192.168.88.1 est toujours indisponible."
> done; echo "192.168.88.1 est disponible."
```

c: signifie le nombre de ping
W: signifie le temps qu'il attend

Tant que l'hôte ne répond pas, il le ping. Bon ça va virer à une belle boucle infinie, mais on peut comprendre l'intérêt. Le until est souvent utilisé pour attendre qu'un truc devient fonctionnel avant de continuer.

[Syntaxe](Syntaxe.md)