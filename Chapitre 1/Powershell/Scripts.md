# Scripts

La première étape de création de script est de créer un fichier possédant une extension ps1. Cette extension indiquera au système d'exploitation qu'il s'agit en réalité d'un script powershell. Encore ici, une entête se voudrait utile:

Deux façons existent pour commenter en powershell
```Powershell
<#
Ceci est une zone multiligne de commentaire
#>
Write-Host "patate" # Ceci est comme un echo
```

Une bonne en-tête de fichier contiendrait les informations suivantes:

```Powershell
<#
Filename:
Description:
Arguments:
Auteur:
Variable modifiable:
#>
```

## Exécution de script
Pour être exécuté, on doit changer la politique d'exécution de script. Il y a 4 statut possible de cette politique

* Restricted: Aucun script ne peut être exécuté, c'est le mode par défaut. Par conséquent, si vous voulez testez vos scripts, vous devrez changer la valeur de ceci.
* AllSigned: Vous ne pouvez qu'exécuter un script n'ayant été codé que par un programmeur de confiance. Vous serez amené à confirmer l'exécution à chaque fois.
* RemoteSigned: Vous pouvez exécuter vos propres scripts ou les scripts signés par un programmeur de confiance.
* Unrestricted: Vous pouvez exécuter n'importe quel script.
Pour voir l'état de votre politique actuelle, faîtes :

```Powershell
Get-ExecutionPolicy
```
Pour modifier cet état, vous devez faire

```Powershell
Set-ExecutionPolicy RemoteSigned
```

Vous confirmez pour tous les scripts (T), vous serez prêt enfin d'exécuter vos scripts.

## Test
La majorité des Cmdlets PowerShell vont supporter les paramètres suivants:
* -whatif: La commande n'est pas exécutée mais vous permet de voir les impacts si elle l'étaient.
* -confirm:  Demande à l'utilisation avant d'exécuter la cmdlet.
* -verbose: Offre plus d'information générale.
* -debug: Offre de l'information de déboggage.
* -ErrorAction: Indique à PowerShell comment réagir lors d'erreur. Par exemple, voici sur quoi il peut être configuré.: continue, stop, silently continue et inquire.

## Affichage à l'écran
Pour afficher directement en ligne de commande, c'est un peu comme un echo.

```Powershell
Write-Output "hello"
```

Il accepte comme echo l'affichage des variables:

```Powershell
$source = "patate"
# Donc $source va être égal à "patate"
Write-Ouptut "$source"  # affiche patate (comme echo)
```

Alors que 
```Powershell
Write-Output"`$source = $source" # affiche $source = patate
```

Il est également possible dépendamment comment le script est appelé. Il est possible de passer un argument -Debug ou -Verbose au script afin de l'exécuter en mode debug (le plus d'affichage possible) ou verbeux (un peu moins d'affichage mais toujours plus que l'affichage standard). Auquel cas, il faudrait à l'intérieur de notre script, être capable d'afficher à l'écran en fonction.

```Powershell
Write-Debug
Write-Verbose 
```

Qui se feront exécutées que lorsque l'appel se fera ainsi, supposons que votre script se nomme patate.ps1

Je pourrais l'exécuter en appelant

    patate.ps1 -Debug 
    patate.ps1 -Verbose

Les appels Write-Debug ou Write-Verbose seront exécutés seulement lorsqu'appeler.


## Redirection
Certaines commandes provoquent des affichages non volontaires car ils retournent un objet. Testez la commande mkdir (ou New-Item)

    New-Item -ItemType Directory patate5
    Répertoire : C:\Users\michel.di.croci\Test
    Mode             LastWriteTime       Length Name
    ----             -------------       ------ ----
    d---             2020-05-03 10:50           patate5
Comment empêcher la sortie d'autant de contenu dans nos scripts?

En utilisant la commande Out, nous avons vu le Out-File lorsque nous nous amusions avec le type de retour vers un fichier. Il est aussi disponible pour rediriger la sortie vers null (qui comme sous bash permet d'empêcher l'afficher). 

Bon p-e nous ne l'avons pas vu sous bash, mais ça existe echo "patate" > /dev/null va rediriger la sortie vers le null donc il n'y aura pas de sortie.

Ainsi, si nous modifions la commande précédente 

```Powershell
New-Item -ItemType Directory patate5 | Out-Null
New-Item : Il existe déjà un élément avec le nom spécifié C:\Users\michel.di.croci\Documents\cours\420-M24 Systeme
d''exploitation II\Solutionnaire\patate5.
Au caractère Ligne:1 : 1
+ New-Item -ItemType Directory patate5 | Out-Null
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : ResourceExists: (C:\Users\michel...onnaire\patate5:String) [New-Item], IOException
    + FullyQualifiedErrorId : DirectoryExist,Microsoft.PowerShell.Commands.NewItemCommand
```

Qui s'explique que le répertoire existe déjà, ce qui est intéressant de constater est que les messages d'erreur sont toujours présents.

Il est à noter qu'on peut utiliser les mêmes redirections qu'en bash et que c'est fonctionnel:

    commande 2> $null # sortie erreur
    commande > $null # sortie standard
    commande 2&>1 | Out-Null # Sortie erreur redirigée dans sortie standard et au final Out-Null

## Variables déclarées par le shell

```Powershell
$_ # Contient l'objet en cours lorsqu'on utilise le pipe.
$Args # Contient un tableau des données passées en paramètres à une fonction PowerShell.
$Errors # Contient les objets ayant généré des erreurs lors de l'exécution de la cmdlet.
$Home # Contient le répertoire de l'utilisateur.
$PsHome # Contient le répertoire où est installé PowerShell.
$Env # Contient les variables d'environnement.
```

Exemple:
```Powershell
write-host $Args[1] va retourné le 2ième argument
write-host $env:windir
cd $Home # va aller au répertoire personnel de l'utilisateur
```
