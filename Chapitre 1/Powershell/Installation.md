# Installation #

Powershell est installé par défaut sur Windows 10, plusieurs façons de l'exécuter mais la plus simple demeure clique droit sur logo de Windows et choisir Windows powershell. Vous avez remarqué qu'il y a deux modes d'exécution, administrateur ou non. En ayant les droits administrateurs, vous auriez accès à des portions qui ne sont pas offertes en mode utilisateur, ou vous auriez le droit de modifier des portions de registre inaccessible autrement... ;)

PowerShell 5 est inclus depuis Windows 10, mais une version libre a été rendue publique et disponible par Microsoft , nommée Powershell 7 et pour toutes les plateformes (Windows, Linux, macOS, ...) par Microsoft: https://github.com/PowerShell/PowerShell. Elle ne contient pas toutes les possibilités de sa grande soeur, car elle n'est pas rattachée au FrameWork .NET, mais bien au .NET core. Vous n'aurez donc pas accès à toutes les fonctionnalités de PowerShell qui sont liés à Windows et son coeur, mais vous aurez accès à une grande partie du langage malgré tout.

Powershell Windows vous permet d'accéder à différentes sources de données tel que la base de registre ou le système de fichier de Windows, cependant pour obtenir des accès plus élevés, vous devez exécuter Powershell en mode administrateur. 

### Mise à jour de l'aide ###
Exécuter Powershell en tant qu'administrateur et exécutez la commande suivante pour mettre à jour l'aide locale sur votre ordinateur:

```powershell
Update-Help
```

Cela vous donnera accès à l'aide en ligne de commande de powershell, car vous allez éviter de perdre de l'espace disque si vous n'utilisez jamais powershell. Il peut y avoir des messages d'erreur à la fin, ne vous en faîtes pas, pour la majorité des applications utilisées, l'aide sera suffisante.

[Documentation Microsoft](https://docs.microsoft.com/en-us/powershell/module/?view=powershell-7.1)

[Introduction](Introduction.md)



