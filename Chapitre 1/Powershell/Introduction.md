# Introduction 

## Cmdlet

Les cmdlet sont les commandes-let  et sont composés de deux parties:
  * une **action** (verbe)
  * un **nom** (sur quoi?)
  * séparé par un tiret

```powershell
Get-Member
Get-ChildItem
Set-Location
```

Les verbes sont les actions, par exemple:
* Get
* Start
* Stop
* Out
* Set
* New (pas un verbe mais faisons semblant)
* ...

Les noms sont sur l'objet sur lequel on agit:
* Process
* Service
* ChildItem (fichiers)
* Help
* Command

Donc si vous souhaitez savoir la liste des commandes qu'il est possible d'exécuter en powershell, quelle commande utiliseriez-vous?

```Powershell
Get-Command
```

Si vous souhaitez savoir la liste des commandes qui concerne les processus?

```Powershell
Get-Command -Noun Process
```
L'argument -Noun permet de spécifier ce sur quoi on cherche, bon j'avoue que c'est pas le terme le plus précis, mais c'est celui-là qui est utilisé ;)

Un autre commande très importante est le :
 
```Powershell
Get-Help Get-ChildItem
``` 

Cette commande nous permet d'obtenir de l'aide sur une commande, c'est le man de powershell. Donc ceci va nous donner les détails d'exécution de Get-ChildItem qui est en gros le dir ou le ls traditionnel.

Faîtes le test, exécutez chacune des deux commandes suivantes et comparer la sortie:

 * dir
 * get-childitem

On peut constater que les deux commandes donnent le même résultat, car après tout c'est exactement la même commande qui est exécutée. Mais où se situe cette commande? Puisqu'aucun argument n'a été passé, il s'effectue dans le répertoire en cours. Dans le répertoire utilisateur si le programme Powershell a été exécuté en mode utilisateur, sinon vous êtes dans le répertoire *C:\Windows\system32* en mode administrateur.

```Powershell
dir c:\
```

Nous allons obtenir la liste des "objets" disponible dans le répertoire C:\

Quels sont les types d'objets qu'on peut obtenir lors de l'exécution d'une commande, pour se simplifier la vie par rapport au type d'objet des systèmes de fichier, nous allons nous concentrer vers la sortie de Get-Process:

```Powershell
Get-Process 
```

Nous retourne la liste des processus en cours sous forme de tableau avec des colonnes par défaut et les lignes représentant chacun un processus.

## Aide ##

La commande Get-Help Get-Process -detailed nous indique quel paramètres sont disponibles.

Voici ce qu'elle retourne: 


    NAME
      Get-Process

    SYNOPSIS
      Gets the processes that are running on the local computer or a remote computer.


    SYNTAX
      Get-Process [[-Name] <System.String[]>] [-ComputerName <System.String[]>] [-FileVersionInfo] [-Module] [<CommonParameters>]

      Get-Process [-ComputerName <System.String[]>] [-FileVersionInfo] -Id <System.Int32[]> [-Module] [<CommonParameters>]

      Get-Process [-ComputerName <System.String[]>] [-FileVersionInfo] -InputObject <System.Diagnostics.Process[]> [-Module] [<CommonParameters>]

      Get-Process -Id <System.Int32[]> -IncludeUserName <CommonParameters>]

      Get-Process [[-Name] <System.String[]>] -IncludeUserName [<CommonParameters>]

      Get-Process -IncludeUserName -InputObject <System.Diagnostics.Process[]> [<CommonParameters>]


Donc comme vous pouvez le constater la commande peut être exécutée locale ou sur un ordinateur distant (Computer Name)

Lors de l'exécution, on ne voit qu'un résumé des informations qu'il est possible d'afficher.

```Powershell
Get-Process
```

Comment savoir quels paramètres sont possibles d'afficher lors de l'exécution de la commande?

Get-Member est la commande qui vous permet de savoir les propriétés et les membres d'un objet.

Get-Process | Get-Member -MemberType Property

Vous reconnaissez sans doute le | qui est l'objet utiliser pour prendre les objets de Get-Process et les envoyer vers l'entrée de Get-Member, comme sur Bash.

Donc ceci va nous permettre de savoir les différentes propriétés des données retournées par Get-Process.

Voici la sortie:

    TypeName : System.Diagnostics.Process
    Name                       MemberType Definition
    ----                       ---------- ----------
    BasePriority               Property   int BasePriority {get;}
    Container                  Property   System.ComponentModel.IContainer Container {get;}
    EnableRaisingEvents        Property   bool EnableRaisingEvents {get;set;}
    ExitCode                   Property   int ExitCode {get;}
    ExitTime                   Property   datetime ExitTime {get;}
    Handle                     Property   System.IntPtr Handle {get;}
    HandleCount                Property   int HandleCount {get;}
    HasExited                  Property   bool HasExited {get;}
    Id                         Property   int Id {get;}
    MachineName                Property   string MachineName {get;}
    MainModule                 Property   System.Diagnostics.ProcessModule MainModule {get;}
    MainWindowHandle           Property   System.IntPtr MainWindowHandle {get;}
    MainWindowTitle            Property   string MainWindowTitle {get;}
    MaxWorkingSet              Property   System.IntPtr MaxWorkingSet {get;set;}
    MinWorkingSet              Property   System.IntPtr MinWorkingSet {get;set;}
    Modules                    Property   System.Diagnostics.ProcessModuleCollection Modules {get;}
    NonpagedSystemMemorySize   Property   int NonpagedSystemMemorySize {get;}
    NonpagedSystemMemorySize64 Property   long NonpagedSystemMemorySize64 {get;}
    PagedMemorySize            Property   int PagedMemorySize {get;}
    PagedMemorySize64          Property   long PagedMemorySize64 {get;}
    PagedSystemMemorySize      Property   int PagedSystemMemorySize {get;}
    PagedSystemMemorySize64    Property   long PagedSystemMemorySize64 {get;}
    PeakPagedMemorySize        Property   int PeakPagedMemorySize {get;}
    PeakPagedMemorySize64      Property   long PeakPagedMemorySize64 {get;}
    PeakVirtualMemorySize      Property   int PeakVirtualMemorySize {get;}
    PeakVirtualMemorySize64    Property   long PeakVirtualMemorySize64 {get;}
    PeakWorkingSet             Property   int PeakWorkingSet {get;}
    PeakWorkingSet64           Property   long PeakWorkingSet64 {get;}
    PriorityBoostEnabled       Property   bool PriorityBoostEnabled {get;set;}
    PriorityClass              Property   System.Diagnostics.ProcessPriorityClass PriorityClass {get;set;}
    PrivateMemorySize          Property   int PrivateMemorySize {get;}
    PrivateMemorySize64        Property   long PrivateMemorySize64 {get;}
    PrivilegedProcessorTime    Property   timespan PrivilegedProcessorTime {get;}
    ProcessName                Property   string ProcessName {get;}
    ProcessorAffinity          Property   System.IntPtr ProcessorAffinity {get;set;}
    Responding                 Property   bool Responding {get;}
    SafeHandle                 Property   Microsoft.Win32.SafeHandles.SafeProcessHandle SafeHandle {get;}
    SessionId                  Property   int SessionId {get;}
    Site                       Property   System.ComponentModel.ISite Site {get;set;}
    StandardError              Property   System.IO.StreamReader StandardError {get;}
    StandardInput              Property   System.IO.StreamWriter StandardInput {get;}
    StandardOutput             Property   System.IO.StreamReader StandardOutput {get;}
    StartInfo                  Property   System.Diagnostics.ProcessStartInfo StartInfo {get;set;}
    StartTime                  Property   datetime StartTime {get;}
    SynchronizingObject        Property   System.ComponentModel.ISynchronizeInvoke SynchronizingObject {get;set;}
    Threads                    Property   System.Diagnostics.ProcessThreadCollection Threads {get;}
    TotalProcessorTime         Property   timespan TotalProcessorTime {get;}
    UserProcessorTime          Property   timespan UserProcessorTime {get;}
    VirtualMemorySize          Property   int VirtualMemorySize {get;}
    VirtualMemorySize64        Property   long VirtualMemorySize64 {get;}
    WorkingSet                 Property   int WorkingSet {get;}
    WorkingSet64               Property   long WorkingSet64 {get;}


Remarquez qu'en haut de la commande, il y a un typeName. C'est l'objet .NET qui est retourné, comme dit précédemment, powershell utilise des objets .NET nous permettant donc d'avoir accès à plein de propriété et de méthode par les objets qui nous sont ainsi retournés. 

Vous ne devez pas apprendre ça par coeur, mais ça vous laisse voir avec quelle facilité et combien de propriété on a accès.

Un exemple sur le site de Microsoft est également intéressant: lien

```Powershell
$File = Get-Item c:\test\textFile.txt
$File.PSObject.Properties | Where-Object isSettable | Select-Object -Property Name
```

    Name
    ----
    PSPath
    PSParentPath
    PSChildName
    PSDrive
    PSProvider
    PSIsContainer
    IsReadOnly
    CreationTime
    CreationTimeUtc
    LastAccessTime
    LastAccessTimeUtc
    LastWriteTime
    LastWriteTimeUtc
    Attributes


## Sélection, regroupement, tri des résultats et filtre ##

### Sélection

Désormais pour pouvoir gérer quelles données sont affichées, nous utiliserons la commande Select-Object:

```Powershell
Get-Process | Select-Object Id, ProcessName, StartTime
```

Permet d'obtenir la liste des processus mais contraint la sortie à afficher le numéro d'identification (ID), le nom de processus et le moment où le processus a été démarré. Pour pouvour être sélectionné ainsi, ce doit être des propriétés.

Le cours ne porte pas sur .NET et ne nécessitera pas une profonde compréhension du langage. Simplement savoir comment faire appel à des méthodes ainsi. Donc en gros (Get-Date) retourne un objet de type Date et comme aucun paramètre lui est envoyé, il retourne la date d'aujourd'hui:

PS C:\Windows\system32> get-date
24 avril 2020 10:34:39
Mais ceci est la sortie textuel de l'objet de type

```Powershell
PS C:\Windows\system32> Get-Date | Get-Member
```
Retourne l'information suivante

    TypeName : System.DateTime
    Name                 MemberType     Definition
    ----                 ----------     ----------
    Add                  Method         datetime Add(timespan value)
    AddDays              Method         datetime AddDays(double value)
    AddHours             Method         datetime AddHours(double value)
    AddMilliseconds      Method         datetime AddMilliseconds(double value)
    AddMinutes           Method         datetime AddMinutes(double value)
    AddMonths            Method         datetime AddMonths(int months)
    AddSeconds           Method         datetime AddSeconds(double value)
    AddTicks             Method         datetime AddTicks(long value)
    AddYears             Method         datetime AddYears(int value)
    ...
    Date                 Property       datetime Date {get;}
    Day                  Property       int Day {get;}
    DayOfWeek            Property       System.DayOfWeek DayOfWeek {get;}
    DayOfYear            Property       int DayOfYear {get;}
    Hour                 Property       int Hour {get;}
    Kind                 Property       System.DateTimeKind Kind {get;}
    Millisecond          Property       int Millisecond {get;}
    Minute               Property       int Minute {get;}
    Month                Property       int Month {get;}
    Second               Property       int Second {get;}
    Ticks                Property       long Ticks {get;}
    TimeOfDay            Property       timespan TimeOfDay {get;}
    Year                 Property       int Year {get;}

On voit donc que l'objet qui est de type System.DateTime, on peut voir les différentes propriétés et méthodes disponibles. Si vous ne savez pas la différence entre les deux, la méthode est une façon de modifier les propriétés de vos objets. 

Donc dans notre cas, AddDays permet de modifier la propriété Date de l'objet DateTime retourné. Ainsi, nous somme capable d'ajouter ou de supprimer des journées de la Date retournée. Par défaut, Get-Date retourne la date d'aujourd'hui. Si nous revenons à notre commande, donc nous soustrayons une journée de la date d'aujourd'hui et on tombe sur hier :

```Powershell
$StartTime=(Get-Date).AddDays(-1)
$EndTime=(Get-Date)
```

On soustrait une journée à celle d'aujourd'hui qu'on passe à la commandlet Get-WinEvent. En gros, on obtient la liste des logs d'applications qui se sont produits depuis hier. Voilà toute la puissance du powershell. Pour la gestion de plusieurs serveurs en ligne, ça nous permet rapidement de produit des résultats et de gérer nos propres requêtes. Les arguments permettent également de choisir les applications qu'on voudrait filtrer.

Il est toujours possible d'exporter ce résultat vers un formattage différent:

```Powershell
Get-WinEvent -FilterHashtable @{LogName='Application'; StartTime=$StartTime; EndTime=$EndTime; } -MaxEvents 5 | Select-Object TimeCreated, ID, Message
```

## Group, Sort et Filtre ## 

### Group
Il est possible de regrouper les données lorsqu'il y a plusieurs données répétant les mêmes lignes dans un tableau. La commande Group-Object vous permettra de regrouper les éléments ayant plusieurs lignes similaires.

Get-ChildItem C:\ | Group-Object Extension

### Sort

La commande Sort-Object vous permettra de trier les éléments selon la colonne spécifiée.

Get-Process | Sort-Object VM -DESC | SELECT-OBJECT ProcessName, VM

### Where

La commande Where-Object vous permettra de filtre sur les éléments des éléments lui étant passés.

```Powershell
Get-Process | Where-Object {$_.ProcessName -Match "^a"}
Get-Process | Where-Object ProcessName -Match "^a"
Get-Process | where ProcessName -Match "^a"                
```

Le Match reçoit un regex ou un pattern permettant d'identifier un processus. Le premier cas est le plus ancien, il représente que pour chacun des objets, je boucle sur l'item $_ (l'item en cours) et je valide qu'il correspond au match. Mais les 3 méthodes sont correctes syntaxiquement.

Il se peut que dans certains cas la propriété n'existe pas et que vous soyez contraints d'utiliser le $_ qui nous permet donc d'obtenir chacun des objets de la liste.

### Comparaison dans le where

Ils sont surtout en conjugaison avec le where dans une comparaison:

|  Opérateurs |  Description  |
| ---  | --- |
| -eq, -ne | égal (eq) ou différent (ne) |
| -lt, -gt, -le, -ge | plus petit, plus grand, plus petit ou égal, plus grand ou égal |
| -match  |  expression régulière à matcher après.  |

Par exemple, la commande suivante liste les fichiers situé dans le répertoire en cours ou sous-répertoire d'une taille plus grande que 100MB. Le résultat est affiché ligne par ligne comportant le répertoire et le nom:

```Powershell
dir -recurse | where length -gt 100MB | select directoryName, name
```

### Affichage et exportation des données

4 modes d'affichage, celui qu'on a vu à date c'est le premier Format-Table, mais il est possible d'afficher les résultats de façon distincte à l'écran:

* Format-Table, données tabulaires
* Format-Wide, données pour fitter l'écran
* Format-List, données listées (offrent plus de détail que les deux premiers affichages
* Format-Custom

N'hésitez pas à tester avec:
```Powershell
dir | Format-Table
dir | Format-Wide
dir | Format-List
```

### Écriture des données
Il est possible d'exporter les données retournées d'un script powershell (comme dir qu'on a vu précédemment vers un autre format:

* ConvertTo-Html
* ConvertTo-CSV
* ConvertTo-Json
* ConvertTo-Xml
 
Les 4 méthodes vont retourner les données vers un de ces formats. Par contre, il ne seront pas inscrites dans un fichier mais seulement converties vers un formattage différent.

Si nous voulons sauvegarder la sortie vers un de ces 4 format, vous devez utiliser la commande à la fin de votre ConvertTo:

```Powershell
ConvertTo-Html | Out-File fichier.html
ConvertTo-CSV | Out-File fichier.csv
ConvertTo-Json | Out-File fichier.json
ConvertTo-Xml | Out-File fichier.xml
```

Exécutez la commande suivante:

```Powershell
dir C:\ | ConvertTo-Html | Out-File  C:\patate.html
```

Cela devrait créer un fichier nommé patate.html, contenant la commande dir du dossier c:\.
Il aurait été possible aussi d'exécuter une de nos requêtes précédentes et de modifier la sortie.
```Powershell
Get-WinEvent -FilterHashtable @{LogName='Application'; StartTime=$StartTime; EndTime=$EndTime; } -MaxEvents 5 | Select-Object TimeCreated, ID, Message | ConvertTo-Json | Out-File C:\Application.json
```

## Alias 

Au début du cours, j'ai utilisé la commande dir, n'avez-vous pas été surpris de constater qu'elle existait en Powershell comme sous DOS (cmd)? Si votre réponse est non, car elle existe en ligne de commande ordinaire, vous vous trompez. Ce n'est pas une cmdlet disponible sur Powershell. Les commandes DOS ordinaires et plusieurs commandes Linux (ls, cp, mv) sont *disponibles* en Powershell, car ce sont des alias pointant vers la bonne syntaxe powershell.

Que fait un **dir**? 

Ça retourne les fichiers / répertoire du répertoire en cours

Que fait un **ls**? 

Ça retourne les fichiers / répertoires du répertoire en cours

Voyons sous powershell comment obtenir cette information? Certains seraient tentés de dire Get-Alias?

    Get-Alias dir:
    dir -> Get-ChildItem

    Get-Alias -definition Get-ChildItem:
    Alias           dir -> Get-ChildItem
    Alias           gci -> Get-ChildItem
    Alias           ls -> Get-ChildItem

J'ai procédé en deux étapes, la première étant de pouvoir voir quelle commande était appelée en réalité lorsqu'on tapait la commande dir. La deuxième, en connaissant la commande appelée, est-ce que je pouvais voir toutes les commandes rattachées à Get-ChildItem.

On peut donc constater que dir et ls pointe les deux à Get-ChildItem qui est le nom de la commande Powershell permettant de voir les fichiers du répertoire en cours.

| Commande | Définition | 
| --- | --- |
| Get-Alias | permet de voir la liste de tous les alias en cours d'utilisation |
| Set-Alias |  vous permettra d'enregistrer vos propres alias |
| Export-Alias -Path Aliases.txt | exportera la liste complète de vos alias vers le fichier Aliases.txt | 
| Import-Alias -Path Aliases.txt | importera la liste complète de vos alias depuis le fichier Aliases.txt |

```Powershell
Set-Alias gs Get-Service

gs
```

[Installation](Installation.md)   [Provider](Provider.md)