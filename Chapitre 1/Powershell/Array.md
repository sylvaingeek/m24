# Array

Un autre type que vous devrez maîtriser est le array (ou tableau). 

Voici quelques façons d'initialiser un array:

```Powershell

# Array vide

$A = @() 
$A.count
0

$A = @(22,5,10,8,12,9,80)

# Autre assignation fonctionnelle mais quelque peu spéciale, à oublier dans les scripts 

$A = 22,5,10,8,12,9,80

# Sinon

$A = 1..3    #Array comme en BASH
```

Voyons les propriétés de $A
```Powershell
$A | Get-Member

# Malheureusement cette méthode retourne les éléments de l'array, donc on reçoit System.Int32

TypeName: System.Int32

Name        MemberType Definition
----        ---------- ----------
CompareTo   Method     int CompareTo(System.Object value), int CompareTo(int value), int IComparable.CompareTo(System.Object obj), int IComparable[int].CompareTo(int other)
Equals      Method     bool Equals(System.Object obj), bool Equals(int obj), bool IEquatable[int].Equals(int other)
GetHashCode Method     int GetHashCode()
GetType     Method     type GetType()
GetTypeCode Method     System.TypeCode GetTypeCode(), System.TypeCode IConvertible.GetTypeCode()
ToBoolean   Method     bool IConvertible.ToBoolean(System.IFormatProvider provider)
ToByte      Method     byte IConvertible.ToByte(System.IFormatProvider provider)
ToChar      Method     char IConvertible.ToChar(System.IFormatProvider provider)
ToDateTime  Method     datetime IConvertible.ToDateTime(System.IFormatProvider provider)
ToDecimal   Method     decimal IConvertible.ToDecimal(System.IFormatProvider provider)
ToDouble    Method     double IConvertible.ToDouble(System.IFormatProvider provider)
ToInt16     Method     int16 IConvertible.ToInt16(System.IFormatProvider provider)
ToInt32     Method     int IConvertible.ToInt32(System.IFormatProvider provider)
ToInt64     Method     long IConvertible.ToInt64(System.IFormatProvider provider)
ToSByte     Method     sbyte IConvertible.ToSByte(System.IFormatProvider provider)
ToSingle    Method     float IConvertible.ToSingle(System.IFormatProvider provider)
ToString    Method     string ToString(), string ToString(string format), string ToString(System.IFormatProvider provider), string ToString(string format, System.IFormatProvider provider), string IFormatta...
ToType      Method     System.Object IConvertible.ToType(type conversionType, System.IFormatProvider provider)
ToUInt16    Method     uint16 IConvertible.ToUInt16(System.IFormatProvider provider)
ToUInt32    Method     uint32 IConvertible.ToUInt32(System.IFormatProvider provider)
ToUInt64    Method     uint64 IConvertible.ToUInt64(System.IFormatProvider provider)

```

Pour obtenir la bonne réponse,

```Powershell
Get-Member -InputObject $A

   TypeName: System.Object[]

Name           MemberType            Definition
----           ----------            ----------
Count          AliasProperty         Count = Length
Add            Method                int IList.Add(System.Object value)
Address        Method                System.Object&, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089 Address(int )
Clear          Method                void IList.Clear()
Clone          Method                System.Object Clone(), System.Object ICloneable.Clone()
CompareTo      Method                int IStructuralComparable.CompareTo(System.Object other, System.Collections.IComparer comparer)
Contains       Method                bool IList.Contains(System.Object value)
CopyTo         Method                void CopyTo(array array, int index), void CopyTo(array array, long index), void ICollection.CopyTo(array array, int index)
Equals         Method                bool Equals(System.Object obj), bool IStructuralEquatable.Equals(System.Object other, System.Collections.IEqualityComparer comparer)
Get            Method                System.Object Get(int )
GetEnumerator  Method                System.Collections.IEnumerator GetEnumerator(), System.Collections.IEnumerator IEnumerable.GetEnumerator()
GetHashCode    Method                int GetHashCode(), int IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer comparer)
GetLength      Method                int GetLength(int dimension)
GetLongLength  Method                long GetLongLength(int dimension)
GetLowerBound  Method                int GetLowerBound(int dimension)
GetType        Method                type GetType()
GetUpperBound  Method                int GetUpperBound(int dimension)
GetValue       Method                System.Object GetValue(Params int[] indices), System.Object GetValue(int index), System.Object GetValue(int index1, int index2), System.Object GetValue(int index1, int ...
IndexOf        Method                int IList.IndexOf(System.Object value)
Initialize     Method                void Initialize()
Insert         Method                void IList.Insert(int index, System.Object value)
Remove         Method                void IList.Remove(System.Object value)
RemoveAt       Method                void IList.RemoveAt(int index)
Set            Method                void Set(int , System.Object )
SetValue       Method                void SetValue(System.Object value, int index), void SetValue(System.Object value, int index1, int index2), void SetValue(System.Object value, int index1, int index2, in...
ToString       Method                string ToString()
Item           ParameterizedProperty System.Object IList.Item(int index) {get;set;}
IsFixedSize    Property              bool IsFixedSize {get;}
IsReadOnly     Property              bool IsReadOnly {get;}
IsSynchronized Property              bool IsSynchronized {get;}
Length         Property              int Length {get;}
LongLength     Property              long LongLength {get;}
Rank           Property              int Rank {get;}
SyncRoot       Property              System.Object SyncRoot {get;}

```

Un coup d'oeil rapide nous confirme le précédent

    $A.GetType()
    IsPublic IsSerial Name BaseType
    -------- -------- ---- --------
    True     True     Object[]                                 System.Array

Ceci nous confirme qu'il s'agit bien d'un array.

Essayons une méthode pour le plaisir
```Powershell
$A.Add(3)

# Erreur les array ne sont pas mutables, on ne peut pas les modifier.

Exception calling "Add" with "1" argument(s): "Collection was of a fixed size."
At line:1 char:1
+ $A.add(3)
+ ~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:) [], MethodInvocationException
    + FullyQualifiedErrorId : NotSupportedException
```

Nous devrions alors utiliser un meilleur objet de collection, soit une liste, soit un ArrayList qui nous donnera la possiblité de passer outre cette limitation. Regardons l'arrayList:
```Powershell
PS > $myarray = New-Object System.Collections.ArrayList
PS > $myarray.add(3)
0
PS > $myarray.add(4)
1
PS > $myarray.add(5)
2
PS > $myarray
3
4
5
```

Reproduit le comportement désiré, permet la modification d'array... Cependant en recherchant sur l'objet, vous noterez que Microsoft suggère largement de ne pas les utiliser [lien](https://docs.microsoft.com/en-us/dotnet/api/system.collections.arraylist?view=netframework-4.7.2). Il faudra alors se tourner vers la liste<typée>.

```Powershell
PS > $mylist = New-Object System.Collections.Generic.List[int]
PS > $mylist.add(1)
PS > $mylist.add(2)
PS > $mylist.add(3)
PS > $mylist
1
2
3
```

## Opérateur d'array

Les arrays supportent les opérations suivantes:
* \+
* +=

Pourquoi avoir rechercher une autre façon de faire le add et comprendre que certaines méthodes ne seront pas fonctionnelles avec les arrays, mais nous constatons que les arrays supportent le + et le - ???

```Powershell
PS > $a=@(1,2,3)
PS > $b=@(1,4,8)

$a+$b
1
2
3
1
4
8

$a+=$b
$a
1
2
3
1
4
8
```

De nouveaux arrays sont créés à chaque fois, dans le premier cas, retourné et affiché. Dans le second cas, le $a initial n'existe plus et est remplacé par un nouveau $a qui est l'addition du $a initial et du $b.


Nous l'avons déjà vu, il est possible de créer un array en utilisant le caractère @. Mais en réalité, toutes commandes peut être retournées en utilisant ceci, en gros:

```Powershell
$B = @(get-psdrive)

# Affectera à B le tableau de la liste des disques accessibles par powershell (disque du système de fichier, HKLM, HKCU,Variable, Env, Alias, ...) 

# On est capable de faire des where sur les résultats ou de boucler sur l'ensemble des éléments.

$B | Where Name -eq "C"
Name           Used (GB)     Free (GB)     Provider   Root 
----           ---------     ---------     --------     ----  
C                  87,64        150,21     FileSystem   C:\  
```
    

